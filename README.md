# Ambeyen

Ambeyen is a mod version of [Ambiance](https://packages.ubuntu.com/source/bionic/ubuntu-themes), a default Ubuntu's theme built for Ubuntu 18.04 LTS Bionic Beaver

## Screenshots
![](https://drive.google.com/uc?id=13IKejYeTGsJ9ZiAnhsqiuWB0P1hLBHoR)
![](https://drive.google.com/uc?id=1_OalBcrkxJvsPCusshqInqsdlJhi36EX)
![](https://drive.google.com/uc?id=1BtZVohg2k-bxwPbuOufDiVdYy0fsS5vk)
![](https://drive.google.com/uc?id=1sFzZjX6WNPTBUvwnAWDa3GG3JiTvdZJz)

## Features
* Toolbar & Headerbar similar to good old Ambiance
* Change selected_bg_color from `#f07746` to `#ec6638`
* Better pathbar background color
* Nautilus sidebar gets new look
* No more double title on stock apps
* Fix a line issue on selected tab 
* Ubuntu font set as Gedit context menu
* And many more

## Installing
* Download the package or clone the repository
* Extract the downloaded package to specific folder
* Copy the folder to `~/.themes/` or `/usr/share/themes`
* Open tweak tool and change to Ambeyen
* Enjoy the new theme

## License
This project is licensed under CC-BY-SA-3.0. The rights in the trademarks, logos, service marks of Canonical Ltd, as well as the look and feel of Ubuntu, are not licensed under the Creative Commons license and are subject to the Canonical Trademark Policy. Please see the [LICENSE.md](LICENSE.md) file for more details.